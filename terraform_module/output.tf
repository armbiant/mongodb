output "mongodb_host" {
  value = "${helm_release.mongodb.metadata[0].name}.${helm_release.mongodb.metadata[0].namespace}"
}

output "mongodb_port" {
  value = "27017"
}