variable "namespace" {
  type = string
}

variable "chart_name" {
  type = string
}

variable "chart_version" {
  type    = string
  default = "11.1.1"
}

variable "values" {
  type    = list(string)
  default = []
}

variable "helm_force_update" {
  type    = bool
  default = false
}

variable "helm_recreate_pods" {
  type    = bool
  default = false
}

variable "helm_cleanup_on_fail" {
  type    = bool
  default = false
}

variable "helm_max_history" {
  type    = number
  default = 0
}

variable "limits_cpu" {
  type    = string
  default = "1100m"
}

variable "limits_memory" {
  type    = string
  default = "1024Mi"
}

variable "requests_cpu" {
  type    = string
  default = "500m"
}

variable "requests_memory" {
  type    = string
  default = "200Mi"
}

variable "architecture" {
  type    = string
  default = "standalone"
}

variable "auth_enabled" {
  type    = bool
  default = null
}
variable "auth_rootUser" {
  type    = string
  default = null
}
variable "auth_rootPassword" {
  type    = string
  default = null
}
variable "auth_usernames" {
  type    = list(string)
  default = null
}
variable "auth_passwords" {
  type    = list(string)
  default = null
}
variable "auth_databases" {
  type    = list(string)
  default = null
}
variable "auth_replicaSetKey" {
  type    = string
  default = null
}
variable "auth_existingSecret" {
  type    = string
  default = null
}